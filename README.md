# **Comandos:**

* **Construir a imagem:** docker build -t myapp:v1 .
* **Construir o container:** docker run -d -it --name myapp myapp:v1 ash
* **Executar o container:** docker exec -it myapp ash

# **Comandos para abrir o cv-equipa2.txt automaticamente:**

* **Construir a imagem:** docker build -t myapp:v1 .
* **Executar o container e visualizar o ficheiro:** docker run -it --name myapp myapp:v1
